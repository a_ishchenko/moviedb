import pytest
from django.test import RequestFactory
from rest_framework.test import APIClient

from moviedb.fetcher.api import OmdbApi


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def request_factory() -> RequestFactory:
    return RequestFactory()


@pytest.fixture
def api_client() -> APIClient:
    return APIClient()


@pytest.fixture()
def omdb_client() -> OmdbApi:
    return OmdbApi('TEST_API_KEY')
