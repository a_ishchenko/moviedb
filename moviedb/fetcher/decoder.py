from requests.compat import json as complexjson
from requests.structures import CaseInsensitiveDict


class MovieDecoder(complexjson.JSONDecoder):
    def __init__(self, *args, **kwargs):
        super(MovieDecoder, self).__init__(object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, obj):
        if isinstance(obj, dict):
            return dict(CaseInsensitiveDict(data=obj).lower_items())
        return obj
