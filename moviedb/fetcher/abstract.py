class BaseMovieClient:
    def get_basic_params(self) -> dict:
        raise NotImplementedError(
            f"Method `get_basic_params` of {self.__class__.__name__} not implemented`"
        )

    def get_basic_headers(self) -> dict:
        raise NotImplementedError(
            f"Method `get_basic_headers` of {self.__class__.__name__} not implemented`"
        )

    def fetch_movie_by_title(self, title: str) -> dict:
        raise NotImplementedError(
            f"Method `fetch_movie_by_title` of {self.__class__.__name__} not implemented`"
        )
