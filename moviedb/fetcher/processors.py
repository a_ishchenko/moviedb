import re


class BaseField:
    def __init__(self, value):
        self._value = value


class ListNameProcessor(BaseField):
    def process(self):
        return [{'name': val} for val in self._value.split(',')]


class IntegerProcessor(BaseField):
    def process(self):
        try:
            return int(''.join(re.findall(r'\d+', self._value)))
        except TypeError:
            return None
