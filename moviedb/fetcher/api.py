import requests
from requests.structures import CaseInsensitiveDict

from moviedb.fetcher.constant import MOVIE_NOT_FOUND
from moviedb.fetcher.decoder import MovieDecoder
from moviedb.fetcher.entities import Movie
from .exceptions import MovieNotFound
from .abstract import BaseMovieClient


class OmdbApi(BaseMovieClient):
    BASE_URL = 'http://www.omdbapi.com/'
    CONTENT_TYPE = 'application/json'

    def __init__(self, api_key: str):
        self.api_key = api_key

    def get_basic_params(self) -> dict:
        return {'apikey': self.api_key}

    def get_basic_headers(self) -> dict:
        return {
            'Content-Type': self.CONTENT_TYPE,
            'Accept': self.CONTENT_TYPE,
        }

    def fetch_movie_by_title(self, title: str) -> dict:
        response = requests.get(
            self.BASE_URL,
            params={**self.get_basic_params(), 't': title, 'plot': 'full'},
            headers=self.get_basic_headers()
        )
        response.raise_for_status()
        response_data = dict(CaseInsensitiveDict(response.json(cls=MovieDecoder)).lower_items())
        if response_data.get('error') == MOVIE_NOT_FOUND:
            raise MovieNotFound()
        movie = Movie.from_dict(response_data)
        return movie.to_dict()
