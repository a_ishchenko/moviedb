import pytest
import responses
from requests import HTTPError
from requests.structures import CaseInsensitiveDict

from ..entities import Movie
from ..exceptions import MovieNotFound

json_data = {
    "Title": "Django", "Year": "1966", "Rated": "Not Rated", "Released": "01 Dec 1966", "Runtime": "91 min",
    "Genre": "Action, Western", "Director": "Sergio Corbucci",
    "Writer": "Sergio Corbucci (story), Bruno Corbucci (story), Sergio Corbucci (screenplay), Bruno Corbucci "
              "(screenplay), Franco Rossetti (screenplay in collaboration with), Piero Vivarelli (screenplay in "
              "collaboration with), Geoffrey Copleston (English version by)",
    "Actors": "Franco Nero, José Bódalo, Loredana Nusciak, Ángel Álvarez",
    "Plot": "In the opening scene a lone man walks, behind him he drags a coffin."
            " That man is Django. He rescues a woman from bandits and, later, arrives"
            " in a town ravaged by the same bandits. The scene for confrontation is set."
            " But why does he drag that coffin everywhere and who, or what, is in it?",
    "Language": "Italian", "Country": "Italy, Spain", "Awards": "N/A",
    "Poster": "https://m.media-amazon.com/images/M/MV5BMTA4M2NmZTgtOGJlOS00NDExLWE4MzItNWQxNTRmYzIzYmM0L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SX300.jpg", # noqa
    "Ratings": [{"Source": "Internet Movie Database", "Value": "7.3/10"}, {"Source": "Rotten Tomatoes", "Value": "92%"},
                {"Source": "Metacritic", "Value": "75/100"}], "Metascore": "75", "imdbRating": "7.3",
    "imdbVotes": "22,327", "imdbID": "tt0060315", "Type": "movie", "DVD": "24 Sep 2002", "BoxOffice": "$25,097",
    "Production": "Rialto Pictures", "Website": "N/A", "Response": "True"
}


class TestMovieDataClass:
    def test_data_parsing(self):
        data = dict(CaseInsensitiveDict(json_data).lower_items())
        movie = Movie.from_dict(data)
        assert isinstance(movie, Movie)
        assert movie.to_dict()['title'] == json_data['Title']


class TestFetcher:

    @responses.activate
    def test_normal_fetch(self, omdb_client):
        responses.add(
            responses.GET, f"http://www.omdbapi.com/?apikey={omdb_client.api_key}&t=test_title&plot=full",
            json=json_data, status=200
        )
        movie = omdb_client.fetch_movie_by_title('test_title')
        assert isinstance(movie, dict)

    @responses.activate
    def test_not_found(self, omdb_client):
        responses.add(
            responses.GET, f"http://www.omdbapi.com/?apikey={omdb_client.api_key}&t=test_title&plot=full",
            json={'error': 'Movie not found!'}, status=200
        )
        with pytest.raises(MovieNotFound):
            omdb_client.fetch_movie_by_title('test_title')

    @responses.activate
    def test_http_error_500(self, omdb_client):
        responses.add(
            responses.GET, f"http://www.omdbapi.com/?apikey={omdb_client.api_key}&t=test_title&plot=full",
            status=500
        )
        with pytest.raises(HTTPError):
            omdb_client.fetch_movie_by_title('test_title')

    @responses.activate
    def test_http_error_403(self, omdb_client):
        responses.add(
            responses.GET, f"http://www.omdbapi.com/?apikey={omdb_client.api_key}&t=test_title&plot=full",
            status=403
        )
        with pytest.raises(HTTPError):
            omdb_client.fetch_movie_by_title('test_title')
