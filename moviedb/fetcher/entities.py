from typing import List

from dataclasses import dataclass, field, asdict, InitVar

from moviedb.fetcher.constant import OMDB_API_NULL
from moviedb.fetcher.processors import IntegerProcessor

NESTED_OBJECTS = ('country', 'language', 'genre')


@dataclass(init=True)
class Country:
    name: str

    @staticmethod
    def from_dict(obj: dict) -> 'Country':
        return Country(**obj)


@dataclass(init=True)
class Language:
    name: str

    @staticmethod
    def from_dict(obj: dict) -> 'Language':
        return Language(**obj)


@dataclass(init=True)
class Rating:
    source: str
    value: str
    movie: int = None

    @staticmethod
    def from_dict(obj: dict) -> 'Rating':
        return Rating(**obj)


@dataclass(init=True)
class Genre:
    name: str

    @staticmethod
    def from_dict(obj: dict) -> 'Genre':
        return Genre(**obj)


@dataclass(init=True)
class Movie:
    title: str
    director: str = None
    writer: str = None
    actors: str = None
    plot: str = None
    awards: str = None
    poster: str = None
    metascore: int = None
    imdbrating: str = None
    imdbvotes: str = None
    imdbid: str = None
    type: str = None
    dvd: str = None
    boxoffice: str = None
    production: str = None
    website: str = None
    year: int = None
    rated: str = None
    released: str = None
    runtime: str = None
    response: str = None
    genre: List[Genre] = field(default_factory=Genre.from_dict)
    language: List[Language] = field(default_factory=Language.from_dict)
    country: List[Country] = field(default_factory=Country.from_dict)
    ratings: List[Rating] = field(default_factory=Rating.from_dict)

    post_processors = {
        'metascore': IntegerProcessor,
        'imdbvotes': IntegerProcessor,
        'boxoffice': IntegerProcessor,
    }

    def __post_init__(self):
        for key, processor in self.post_processors.items():
            value = getattr(self, key, None)
            setattr(self, key, processor(value).process())

    @staticmethod
    def from_dict(obj: dict) -> 'Movie':
        params = {}
        for k, v in obj.items():
            if v == OMDB_API_NULL:
                continue
            if k in NESTED_OBJECTS:
                params[k] = [{'name': val} for val in v.split(',')]
            else:
                params[k] = v
        return Movie(**params)

    def to_dict(self) -> dict:
        return asdict(self)
