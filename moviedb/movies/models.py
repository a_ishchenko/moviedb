from django.db import models
from django.db.models import Window, Count, F
from django.db.models.functions import DenseRank

from .constants import PICTURE_TYPES


class MovieQuerySet(models.QuerySet):
    @staticmethod
    def _get_top_movies_window():
        return Window(
            expression=DenseRank(),
            order_by=F("total_comments").desc()
        )

    def top_movies(self):
        return self.annotate(
            total_comments=Count('comments')) \
            .order_by('total_comments') \
            .annotate(rank=self._get_top_movies_window()) \
            .order_by('rank')

    def top_movies_values_list(self):
        return self.top_movies().values_list(
            'id', 'total_comments', 'rank', named=True
        )


class Movie(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=255, unique=True)
    year = models.CharField(max_length=4, null=True)
    rated = models.CharField(max_length=100, null=True)
    released = models.DateField(blank=True, null=True)
    runtime = models.CharField(max_length=500, null=True)
    director = models.CharField(max_length=500, null=True)
    writer = models.CharField(max_length=500, null=True)
    actors = models.CharField(max_length=500, null=True)
    plot = models.TextField(null=True)
    awards = models.CharField(max_length=500, null=True)
    poster = models.URLField(blank=True, null=True)
    metascore = models.IntegerField(blank=True, null=True)
    imdbrating = models.DecimalField(default=0, max_digits=3, decimal_places=1, null=True)
    imdbvotes = models.IntegerField(default=0, null=True)
    type = models.CharField(max_length=50, choices=PICTURE_TYPES)
    dvd = models.DateField(blank=True, null=True)
    boxoffice = models.IntegerField(blank=True, null=True)
    production = models.CharField(max_length=250, null=True)
    website = models.CharField(max_length=250, null=True)

    # Relations
    genre = models.ManyToManyField('Genre')
    country = models.ManyToManyField('Country')
    language = models.ManyToManyField('Language')

    objects = MovieQuerySet.as_manager()

    def __str__(self):
        return f"{self.title}"


class Genre(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class Rating(models.Model):
    source = models.CharField(max_length=250)
    value = models.CharField(max_length=250)
    movie = models.ForeignKey('Movie', related_name='ratings', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.source}: {self.value}"


class Comment(models.Model):
    id = models.BigAutoField(primary_key=True)
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    movie = models.ForeignKey('Movie', related_name='comments', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.body}"


class Country(models.Model):
    name = models.CharField(unique=True, max_length=250)

    def __str__(self):
        return self.name


class Language(models.Model):
    name = models.CharField(unique=True, max_length=250)

    def __str__(self):
        return self.name
