import factory

from .models import Movie, Comment


class MovieFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Movie

    title = factory.Sequence(lambda n: 'movie_%d' % n)


class CommentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Comment

    movie = factory.SubFactory(MovieFactory)
