from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet


class ListCreateViewSet(mixins.CreateModelMixin,
                        mixins.ListModelMixin,
                        GenericViewSet):
    pass


class ListViewSet(mixins.ListModelMixin,
                  GenericViewSet):
    pass


class MovieViewSet(mixins.ListModelMixin,
                   mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.DestroyModelMixin,
                   GenericViewSet):
    pass
