from django.conf.global_settings import DATE_INPUT_FORMATS

PICTURE_TYPES = (
    ('movie', 'Movies'),
    ('series', 'Series'),
)

OMDB_DATE_FORMAT = [
    '%d	%b %Y'
] + DATE_INPUT_FORMATS
