from kombu.exceptions import HttpError
from rest_framework import serializers
from django.conf import settings
from rest_framework.exceptions import ValidationError

from moviedb.fetcher.api import OmdbApi
from moviedb.fetcher.exceptions import MovieNotFound

from .constants import OMDB_DATE_FORMAT
from .models import (
    Movie,
    Genre,
    Rating,
    Comment,
    Language,
    Country
)


class GenreSerializer(serializers.ModelSerializer):
    """
    Genre model serializer class
    """
    class Meta:
        model = Genre
        fields = '__all__'
        extra_kwargs = {
            'name': {
                'validators': [],
            }
        }


class LanguageSerializer(serializers.ModelSerializer):
    """
    Language model serializer class
    """
    class Meta:
        model = Language
        fields = '__all__'
        extra_kwargs = {
            'name': {
                'validators': [],
            }
        }


class CountrySerializer(serializers.ModelSerializer):
    """
    Country model serializer class
    """
    class Meta:
        model = Country
        fields = '__all__'
        extra_kwargs = {
            'name': {
                'validators': [],
            }
        }


class RatingSerializer(serializers.ModelSerializer):
    """
    Rating model serializer class
    """
    movie = serializers.IntegerField(source='movie.id', read_only=True, allow_null=True)

    class Meta:
        model = Rating
        fields = '__all__'


class ListMovieSerializer(serializers.ModelSerializer):
    """
    Serializer is used only for Movie list view, limits data shown on list
    """
    country = serializers.StringRelatedField(many=True)
    language = serializers.StringRelatedField(many=True)
    genre = serializers.StringRelatedField(many=True)

    class Meta:
        model = Movie
        fields = (
            'id', 'title', 'year', 'country', 'language',
            'rated', 'genre', 'plot', 'type', 'poster'
        )


class MovieSerializer(serializers.ModelSerializer):
    """
    Movie serializer for create/detail/update actions
    """
    country = CountrySerializer(allow_null=True, many=True, required=False)
    language = LanguageSerializer(allow_null=True, many=True, required=False)
    genre = GenreSerializer(allow_null=True, many=True, required=False)
    ratings = RatingSerializer(allow_null=True, many=True, required=False)
    released = serializers.DateField(allow_null=True, input_formats=OMDB_DATE_FORMAT)
    dvd = serializers.DateField(allow_null=True, input_formats=OMDB_DATE_FORMAT)

    class Meta:
        model = Movie
        fields = '__all__'

    def pop_related(self, validated_data):
        genres = validated_data.pop('genre', None)
        ratings = validated_data.pop('ratings', None)
        countries = validated_data.pop('country', None)
        languages = validated_data.pop('language', None)
        return genres, ratings, countries, languages

    def update(self, instance, validated_data):
        wont_update = self.pop_related(validated_data)
        for k, v in validated_data.items():
            setattr(instance, k, v)
        return instance

    def create(self, validated_data):
        genres, ratings, countries, languages = self.pop_related(validated_data)
        movie = Movie.objects.create(**validated_data)
        for genre in genres:
            genre, _ = Genre.objects.get_or_create(name=genre['name'])
            movie.genre.add(genre)
        for country in countries:
            country, _ = Country.objects.get_or_create(name=country['name'])
            movie.country.add(country)
        for lang in languages:
            lang, _ = Language.objects.get_or_create(name=lang['name'])
            movie.language.add(lang)
        for rating in ratings:
            rating['movie'] = movie
            Rating.objects.create(**rating)
        return movie


class TopMovieSerializer(serializers.ModelSerializer):
    """
    Serializer for Top movies viewset
    """
    movie_id = serializers.IntegerField(source='id')
    total_comments = serializers.IntegerField()
    rank = serializers.IntegerField()

    class Meta:
        model = Movie
        fields = ('movie_id', 'total_comments', 'rank')


class FetchMovieSerializer(serializers.ModelSerializer):
    """
    Serializer for external api data fetcher
    """
    title = serializers.CharField(required=True, max_length=255)

    class Meta:
        model = Movie
        fields = ('title',)

    @staticmethod
    def _populate_data_from_ext_api(attrs):
        try:
            return OmdbApi(api_key=settings.OMDB_API_KEY) \
                .fetch_movie_by_title(title=attrs['title'])
        except MovieNotFound:
            raise ValidationError({'title': f"Error movie with title: `{attrs['title']}` does not exist"})
        except HttpError:
            raise ValidationError(f"Error while fetching from API, please try again later")

    def validate(self, attrs):
        valid = super(FetchMovieSerializer, self).validate(attrs)
        if valid:
            additional_data = self._populate_data_from_ext_api(attrs)
            attrs.update(additional_data)
        return valid


class CommentSerializer(serializers.ModelSerializer):
    """
    Comment serializer for comments viewset
    """
    class Meta:
        model = Comment
        fields = '__all__'
