from rest_framework import status
from rest_framework.response import Response

from .filters import CommentFilter, TopMovieFilter, MovieFilter
from .helpers import ListCreateViewSet, ListViewSet, MovieViewSet
from .models import Movie, Comment
from .serializers import (
    MovieSerializer, CommentSerializer, TopMovieSerializer, FetchMovieSerializer, ListMovieSerializer
)


class MovieQsMixin:
    queryset = Movie.objects \
        .prefetch_related('language', 'country', 'genre', 'ratings') \
        .all()


class MoviesViewSet(MovieQsMixin, MovieViewSet):
    """
    list:
        Returns list of Movies objects paginated
    retrieve:
        Returns a Movie object
    create:
        Create a movie based on title with data fetched from external API
    delete:
        Removes Movie and it't related objects from database
    partial_update:
        Update a Movie object field
    update:
        Update a Movie object fields
    """
    model = Movie
    serializer_class = MovieSerializer
    create_serializer_class = FetchMovieSerializer
    list_serializer_class = ListMovieSerializer
    filterset_class = MovieFilter

    def get_serializer_class(self):
        if self.action == 'list':
            return self.list_serializer_class
        return self.serializer_class

    def create(self, request, *args, **kwargs):
        serializer = self.create_serializer_class(
            data=request.data,
            context=self.get_serializer_context()
        )
        serializer.is_valid(raise_exception=True)
        serializer = self.get_serializer(data=serializer.validated_data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CommentViewSet(ListCreateViewSet):
    """
    list:
        Returns list of latest comments paginated by `created_at` desc
        Use `movie` query param with movie id value to filter comments by movie
    create:
        Creates new comment to related movie
    """
    queryset = Comment.objects.all().order_by('-created_at')
    serializer_class = CommentSerializer
    filterset_class = CommentFilter


class TopMoviesViewSet(ListViewSet):
    """
    list:
        Should return top movies already present in the database ranking based on a
        number of comments added to the movie (as in the example) in the specified date range.
        The response should include the ID of the movie, position in rank
        and total number of comments (in the specified date range)
    """
    queryset = Movie.objects.all()
    filterset_class = TopMovieFilter
    serializer_class = TopMovieSerializer

    def get_queryset(self):
        return self.queryset.prefetch_related('comments')

    def filter_queryset(self, queryset):
        qs = super(TopMoviesViewSet, self).filter_queryset(queryset)
        return qs.top_movies_values_list()
