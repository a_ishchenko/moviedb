import json

import pytest

from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_204_NO_CONTENT, HTTP_201_CREATED

from ..factory import MovieFactory, CommentFactory


class TestMovieApi:

    @pytest.mark.django_db
    def test_movies_list_empty(self, api_client):
        response = api_client.get(reverse('movie-list'))
        assert response.status_code == HTTP_200_OK

    @pytest.mark.django_db
    def test_movies_list(self, api_client):
        _ = MovieFactory()
        response = api_client.get(reverse('movie-list'))
        assert response.status_code == HTTP_200_OK
        assert len(response.data['results']) > 0

    @pytest.mark.django_db
    def test_movie_details_404(self, api_client):
        response = api_client.get(reverse('movie-detail', kwargs={'pk': 1}))
        assert response.status_code == HTTP_404_NOT_FOUND

    @pytest.mark.django_db
    def test_movie_details_success(self, api_client):
        movie = MovieFactory()
        response = api_client.get(reverse('movie-detail', kwargs={'pk': movie.pk}))
        assert response.status_code == HTTP_200_OK

    @pytest.mark.django_db
    def test_movie_delete(self, api_client):
        movie = MovieFactory()
        response = api_client.delete(reverse('movie-detail', kwargs={'pk': movie.pk}))
        assert response.status_code == HTTP_204_NO_CONTENT

    @pytest.mark.django_db
    def test_movie_delete_404(self, api_client):
        response = api_client.delete(reverse('movie-detail', kwargs={'pk': 1}))
        assert response.status_code == HTTP_404_NOT_FOUND

    @pytest.mark.django_db
    def test_movie_patch(self, api_client):
        movie = MovieFactory()
        response = api_client.patch(reverse('movie-detail', kwargs={'pk': movie.pk}), data={'director': 'TEST1'})
        assert response.status_code == HTTP_200_OK

    @pytest.mark.django_db
    def test_movie_put(self, api_client):
        movie = MovieFactory(type='movie', released='2016-02-02', dvd='2016-02-02')
        response = api_client.get(reverse('movie-detail', kwargs={'pk': movie.pk}))
        new_data = {k: v for k, v in dict(response.data).items() if v != None}
        new_data.update({'director': 'TEST1'})
        response = api_client.put(reverse('movie-detail', kwargs={'pk': movie.pk}), data=new_data)
        assert response.status_code == HTTP_200_OK


class TestCommentsApi:
    @pytest.mark.django_db
    def test_comments_list_empty(self, api_client):
        response = api_client.get(reverse('comment-list'))
        assert response.status_code == HTTP_200_OK

    @pytest.mark.django_db
    def test_comments_list(self, api_client):
        _ = CommentFactory()
        response = api_client.get(reverse('comment-list'))
        assert response.status_code == HTTP_200_OK
        assert len(response.data) > 0

    @pytest.mark.django_db
    def test_create_comment(self, api_client):
        movie = MovieFactory()
        response = api_client.post(reverse('comment-list'), data={'movie': movie.id, 'body': 'test'})
        assert response.status_code == HTTP_201_CREATED

    @pytest.mark.django_db
    def test_create_and_fetch_comment(self, api_client):
        movie = MovieFactory()
        response = api_client.post(reverse('comment-list'), data={'movie': movie.id, 'body': 'test'})
        assert response.status_code == HTTP_201_CREATED
        response = api_client.get(f"{reverse('comment-list')}?movie={movie.id}")
        assert response.status_code == HTTP_200_OK


class TestTopComments:
    @pytest.mark.django_db
    def test_top_empty(self, api_client):
        response = api_client.get(reverse('top-list'))
        assert response.status_code == HTTP_200_OK

    @pytest.mark.django_db
    def test_top_with_data(self, api_client):
        movie = MovieFactory()
        _ = CommentFactory(movie=movie)
        response = api_client.get(reverse('top-list'))
        assert response.status_code == HTTP_200_OK
        assert len(response.data) > 0
        assert dict(response.data['results'][0]) == {'total_comments': 1, 'rank': 1, 'movie_id': movie.id}
