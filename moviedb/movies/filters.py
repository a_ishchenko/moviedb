import django_filters as filters

from .models import Comment, Movie, Genre, Country, Language


class CommentFilter(filters.FilterSet):
    movie = filters.ModelChoiceFilter(lookup_expr='exact', queryset=Movie.objects.all().only('id'))

    class Meta:
        model = Comment
        fields = ('movie',)


class TopMovieFilter(filters.FilterSet):
    date = filters.DateTimeFromToRangeFilter(label='Date range', field_name='comments_created_at')

    class Meta:
        model = Movie
        fields = ('date',)


class MovieFilter(filters.FilterSet):
    genre = filters.ModelMultipleChoiceFilter(queryset=Genre.objects.all())
    language = filters.ModelMultipleChoiceFilter(queryset=Language.objects.all())
    country = filters.ModelMultipleChoiceFilter(queryset=Country.objects.all())

    class Meta:
        model = Movie
        fields = ('genre', 'language', 'country',)
