from django.urls import path, include
from rest_framework import routers

from .api import MoviesViewSet, CommentViewSet, TopMoviesViewSet

router = routers.SimpleRouter()
router.register(r'movies', MoviesViewSet)
router.register(r'comments', CommentViewSet)
router.register(r'top', TopMoviesViewSet, basename='top')

urlpatterns = [
    path('', include(router.urls)),
]
